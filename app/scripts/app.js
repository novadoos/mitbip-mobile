'use strict';

angular.module('starter', [
  'ionic',
  'config',
  'ionic.utils',
  'ngResource',
  'ngCordova',
  'btford.socket-io'
])
  .run(function($ionicPlatform , $rootScope, $state, $localstorage, ENV) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });

    //App interceptor for MVC control
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
      if(toState.data.authenticate && typeof $localstorage.get(ENV.localStorageName) === 'undefined'){
        $state.go('home');
        event.preventDefault();
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'templates/application/home.html',
        controller: 'ApplicationHomeCtrl',
        data:{
          authenticate: false
        }
      })
      .state('splash', {
        url: '/splash',
        templateUrl: 'templates/user/app/home/home.html',
        controller: 'UserAppHomeIndexCtrl',
        data:{
          authenticate: true
        }
      })
      .state('chat', {
        url: '/chat',
        templateUrl: 'templates/user/app/chat/chat.html',
        controller: 'UserAppChatIndexCtrl',
        data:{
          authenticate: false
        }
      });

    $urlRouterProvider.otherwise( function($injector) {
      var $state = $injector.get('$state');
      $state.go('home');
    });
  });