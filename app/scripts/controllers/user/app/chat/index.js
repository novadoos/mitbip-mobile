'use strict';

angular.module('starter')
  .controller('UserAppChatIndexCtrl', function($scope, $state, $ionicPopup, $localstorage, $ionicBackdrop, $ionicScrollDelegate, $cordovaFacebook, Auth, ENV, Socket) {
    var user = $localstorage.getObject(ENV.localStorageObj) || {};

    // This function is just for development purposes (we don't have access to facebook login on web)
    function makeId() {
      var text = '';
      var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }

      return text;
    }

    $scope.clean = function() {
      var conversation = angular.element(document.querySelector('#messages'));
      conversation.html('');
      $scope.userMessage = '';
    };

    $scope.userMessage = '';
    $scope.serverMessage = '';
    $scope.partnerInfo = {
      avatar: '../images/default.png',
      username: 'JUan Pa',
      location: 'Colombia'
    };

    $scope.newRoom = function() {
      Socket.emit('disconnectPartners', user);
    };

    $scope.sendChat = function() {
      Socket.emit('sendChat', $scope.userMessage);
    };

    Socket.on('connect', function() {
      user.username =  user.username ? user.username : user.name ? user.name : makeId();
      Socket.emit('includeUser', user);
    });

    Socket.on('updateChat', function(user, type,  data) {
      var conversation = angular.element(document.querySelector('#messages'));
      conversation.append('<p class="' + type + '" >' + data + '</p>');
      $ionicScrollDelegate.scrollBottom();
    });

    Socket.on('newPartner', function(data) {
      $scope.serverMessage = data;
      $scope.clean();
    });

    Socket.on('waitingByUser', function(data) {
      $scope.serverMessage = data;
    });

    Socket.on('updatePartnerInfo', function(user) {
      var partner = user;
      if (partner.avatar !== '../images/default.png') {
        var index = partner.avatar.indexOf('public/');
        partner.avatar = ENV.apiEndpoint + partner.avatar.substring(index, partner.avatar.length);
      } else {
        partner.avatar = '../images/default.png';
      }
      $scope.partnerInfo = user;
    });
  });
