'use strict';

angular.module('starter')
  .controller('UserAppHomeIndexCtrl', function($scope, $state, $ionicPopup, $localstorage, $ionicBackdrop, $cordovaFacebook, Auth, ENV, CONFIG) {
    $scope.startChatting = function() {
      $state.go('chat');
    };
  });
