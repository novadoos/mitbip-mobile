'use strict';

angular.module('starter')
  .controller('ApplicationHomeCtrl', function($scope, $state, $ionicPopup, $localstorage, $ionicBackdrop, $cordovaFacebook, Auth, ENV, CONFIG) {
    $scope.facebookLogin = function() {
      $ionicBackdrop.retain();
      $cordovaFacebook.login(['public_profile', 'email'])
        .then(function() {
          $cordovaFacebook.getAccessToken()
            .then(function(token) {
              Auth.createFacebookUser({access_token: token})
                .then(function(data) {
                  $ionicBackdrop.release();
                  $localstorage.set(ENV.localStorageName, data._id);
                  $localstorage.setObject(ENV.localStorageObj, data);
                  $state.go('splash');
                })
                .catch(function(err) {
                  console.log(err);
                  $ionicBackdrop.release();
                  $ionicPopup.alert({
                    title: CONFIG.error500Server.title,
                    template: CONFIG.error500Server.msg,
                    okType: 'button-energized'
                  });
                });
            }, function(error) {
              $ionicBackdrop.release();
              console.log(error);
            });
        }, function(error) {
          $ionicBackdrop.release();
          console.log(error);
        });
    };
  });
