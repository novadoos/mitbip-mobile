'use strict';

angular.module('starter')
  .constant('CONFIG', {
    error500Server: {
      title:'Oops',
      msg:'We\'re sorry, but our service is down :(, please try again!'
    },
    success200Server: {
      title:'Nice',
      msg:'It looks good'
    },
    avatarUrl: 'public/users/avatar/'
  });
