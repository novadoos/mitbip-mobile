'use strict';

angular.module('starter')
  .factory('Socket', function (socketFactory, ENV, $localstorage, $rootScope) {
    var myIoSocket = io(ENV.apiEndpoint + '?userId=' + $localstorage.get(ENV.localStorageName), {
      path: '/socket.io-client'
    });
    var mySocket = socketFactory({
      ioSocket: myIoSocket
    });
    mySocket.on('connect', function(){
      $rootScope.socket = this.io.engine.id
    });
    return mySocket;
  });