'use strict';

angular.module('starter')
  .factory('FacebookUser', function($resource, ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/facebook')
  });
