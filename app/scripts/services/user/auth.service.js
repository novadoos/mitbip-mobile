'use strict';

angular.module('starter')
  .factory('Auth', function Auth($rootScope, $localstorage, FacebookUser) {
    return {
      createFacebookUser: function(user, callback) {
        var cb = callback || angular.noop;

        return FacebookUser.save(user,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      }
    };
  });
